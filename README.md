# Twist

[Fault injection](https://en.wikipedia.org/wiki/Fault_injection) for concurrent data structures / synchronization primitives written in C++

## How to use

- `#include <atomic>` → `#include <twist/stdlike/atomic.hpp>`
- `std::atomic<T>` → `twist::stdlike::atomic<T>`

### Twist-ed spinlock

```cpp
#include <twist/stdlike/atomic.hpp>

class SpinLock {
 public:
  void Lock() {
    while (locked_.exchange(true)) {  // <- Faults injected here
      ;  // Backoff
    }
  }

  void Unlock() {
    locked_.store(false);  // <- Faults injected here
  }
 private:
  // Drop-in replacement for std::atomic
  twist::stdlike::atomic<bool> locked_{false};
};
```

## Examples

- [Deadlock](/examples/deadlock)
- [Bounded queue](/examples/bounded_queue)  
- [Semaphore](/examples/semaphore)
- [Lock-Free](/examples/lockfree)
- [TryLock](/examples/try_lock)

## Standard library support

- `atomic`
- `thread`
- `mutex`
- `condition_variable`
- `this_thread::yield`, `this_thread::sleep_for`, `this_thread::get_id`

See [`twist::stdlike`](/twist/stdlike)

## Extensions / Utilities

- `FutexWait` / `FutexWake{One,All}` in atomic
- `util::ThreadLocal<T>` and `util::ThreadLocalPtr<T>`
- `util::SpinWait`

## Layers

| Depth | Layer | Description |
| --- | --- | --- |
| 0 | [`stdlike`](twist/stdlike) | _Facade_ |
| 1 | [`fault`](twist/fault) |  _Fault injection_ |
| 2 | [`strand`](twist/strand) | _Virtual threads (strands)_ |
| 3 | OS threads / [`fiber`](twist/fiber) | _Execution backend_ |

## Three  (almost) orthogonal dimensions

- Fault injection: off / on (`TWIST_FAULTY`)
- Execution backend: threads / [fibers](/twist/fiber/runtime) (`TWIST_FIBERS`)
- Sanitizers: [Address](https://clang.llvm.org/docs/AddressSanitizer.html) / [Thread](https://clang.llvm.org/docs/ThreadSanitizer.html) / none (`ASAN` / `TSAN`)

![Twist](dims.png)

## Fiber execution backend
   
   - Fast context switches (5x as fast as context switch for OS threads)
   - Randomized run queue in scheduler and wait queues in mutexes / condition variables
   - Deterministic execution (deterministic time, randomness)
   - Time compression
   - Compatibility with Address/Thread sanitizers
   - Deadlock detection

## Build profiles

| Name | CMake options | Description |
| --- | --- | --- |
| `FaultyFibers` |  `-DTWIST_FAULTY=ON -DTWIST_FIBERS=ON -DTWIST_PRINT_STACKS=ON` | Fault injection + Fibers execution backend |
| `FaultyThreadsASan` | `-DTWIST_FAULTY=ON -DASAN=ON` | Fault injection + OS threads + Address sanitizer |
| `FaultyThreadsTSan` | `-DTWIST_FAULTY=ON -DTSAN=ON` | Fault injection + OS threads + Thread sanitizer |

## Dependencies

`binutils-dev` package required for `-DTWIST_PRINT_STACKS=ON`

## Research

- [A Randomized Scheduler with Probabilistic Guarantees of Finding Bugs](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/asplos277-pct.pdf)
- [A Practical Approach for Model Checking C/C++11 Code](http://plrg.eecs.uci.edu/publications/toplas16.pdf)

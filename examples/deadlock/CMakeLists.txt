message(STATUS "Twist deadlock example")

# List sources

# All tests target

add_executable(twist_example_deadlock test.cpp)
target_link_libraries(twist_example_deadlock twist)

message(STATUS "Twist semaphore example")

# List sources

# All tests target

add_executable(twist_example_semaphore test.cpp)
target_link_libraries(twist_example_semaphore twist)

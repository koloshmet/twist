#pragma once

#include <twist/stdlike/atomic.hpp>
#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/condition_variable.hpp>

#include <cstdlib>

// Poorly synchronized counting semaphore

class CountingSemaphore {
 public:
  explicit CountingSemaphore(size_t initial_tokens)
      : tokens_(initial_tokens) {
  }

  // -1
  void Acquire() {
    std::unique_lock lock(mutex_);
    while (tokens_.load() == 0) {
      has_tokens_.wait(lock);
    }
    --tokens_;
  }

  // +1
  void Release() {
    //std::lock_guard guard(mutex_);
    ++tokens_;
    has_tokens_.notify_one();
  }

 private:
  twist::stdlike::atomic<size_t> tokens_;
  twist::stdlike::mutex mutex_;
  twist::stdlike::condition_variable has_tokens_;
};

#include <twist/test/test.hpp>

#include <twist/test/util/race.hpp>
#include <twist/test/util/cs.hpp>

#include "semaphore.hpp"

#include <chrono>
#include <iostream>

using namespace std::chrono_literals;

TEST_SUITE(TwistExamples) {
  TWIST_ITERATE_TEST(CountingSemaphore, 3s) {
    const static size_t kThreads = 2;

    CountingSemaphore mutex{1};
    twist::test::util::CriticalSection cs;  // Guarded by mutex

    twist::test::util::Race race;

    for (size_t i = 0; i < kThreads; ++i) {
      race.Add([&]() {
        mutex.Acquire();
        cs();
        mutex.Release();
      });
    }

    race.Run();
  }
}

RUN_ALL_TESTS()

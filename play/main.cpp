#include <twist/stdlike/atomic.hpp>
#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/thread.hpp>

#include <twist/test/run.hpp>

#include <chrono>
#include <vector>
#include <iostream>

int main() {
  twist::Run([]() {
    twist::stdlike::mutex mutex;

    size_t cs = 0;

    std::vector<twist::stdlike::thread> threads;
    for (size_t i = 0; i < 5; ++i) {
      threads.emplace_back([&]() {
        for (size_t j = 0; j < 100; ++j) {
          std::lock_guard g(mutex);
          ++cs;
        }
      });
    }

    for (auto& t : threads) {
      t.join();
    }

    std::cout << "# cs = " << cs << std::endl;
  });

  return 0;
}

#include <twist/test/test.hpp>

#include <twist/strand/stdlike.hpp>

#include <wheels/support/time.hpp>

#include <list>
#include <iostream>

// Virtual threads

namespace stdlike = twist::strand::stdlike;

TEST_SUITE(Strand) {
    SIMPLE_TWIST_TEST(Mutex) {
      stdlike::mutex mutex;
      size_t count = 0;

      static const size_t kIterations = 10;

      auto routine = [&]() {
        for (size_t i = 0; i < kIterations; ++i) {
          stdlike::this_thread::sleep_for(std::chrono::milliseconds(i * 5));

          std::lock_guard lock(mutex);

          ASSERT_FALSE(mutex.try_lock());

          std::cout << "thread " << stdlike::this_thread::get_id()
                    << " in critical section" << std::endl;
          stdlike::this_thread::yield();
          ++count;
          stdlike::this_thread::yield();
        }
      };

      static const size_t kThreads = 5;

      std::list<stdlike::thread> threads;
      for (size_t i = 0; i < kThreads; ++i) {
        threads.emplace_back(routine);
      }
      for (auto& t : threads) {
        t.join();
      }

      ASSERT_EQ(count, kIterations * kThreads);
    }

#if LINUX || defined(TWIST_FIBER)

    SIMPLE_TWIST_TEST(AtomicWait) {
      stdlike::atomic<uint32_t> value{0};

      stdlike::thread waker([&value]() {
        stdlike::this_thread::sleep_for(1s);
        value.notify_one();
      });

      wheels::StopWatch stop_watch;

      // Returns immediately
      value.wait(1);
      ASSERT_TRUE(stop_watch.Elapsed() < 100ms);

      // Blocks for ~1s
      value.wait(0);
      ASSERT_TRUE(stop_watch.Elapsed() > 800ms);

      waker.join();
    }
#endif

}

#include <twist/fault/wrappers/condvar.hpp>

#include <twist/fault/wrappers/mutex.hpp>

#include <wheels/support/compiler.hpp>
#include <wheels/test/test_framework.hpp>

#include <thread>

using namespace std::chrono_literals;
using namespace twist::fault;

#if !defined(FIBER)

// run this tests only with threads

TEST_SUITE(FaultyConditionVariable) {
  SIMPLE_TEST(NotifyOne) {
    FaultyMutex mutex;
    bool ready = false;
    FaultyCondVar condvar;

    auto notify_routine = [&]() {
      std::this_thread::sleep_for(500ms);

      std::lock_guard<FaultyMutex> lock(mutex);
      ready = true;
      condvar.notify_one();
    };

    {
      std::unique_lock<FaultyMutex> lock(mutex);

      std::thread thread(notify_routine);

      condvar.wait(lock, [&]() { return ready; });

      thread.join();
    }
  }

  SIMPLE_TEST(NotifyAll) {
    FaultyMutex mutex;
    size_t count = 5;
    FaultyCondVar all_arrived;

    auto pass_routine = [&]() {
      std::unique_lock<FaultyMutex> lock(mutex);
      --count;
      if (count == 0) {
        all_arrived.notify_all();
      } else {
        while (count > 0) {
          all_arrived.wait(lock);
        }
      }
    };

    std::vector<std::thread> threads;
    for (size_t i = 0; i < 5; ++i) {
      threads.emplace_back(pass_routine);
    }
    for (auto& t : threads) {
      t.join();
    }
  }
}

#endif

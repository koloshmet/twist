#include <twist/fiber/runtime/scheduler.hpp>
#include <twist/fiber/runtime/wait_queue.hpp>

#include <twist/fiber/stdlike/mutex.hpp>
#include <twist/fiber/stdlike/thread.hpp>
#include <twist/fiber/sync/futex.hpp>

#include <wheels/test/test_framework.hpp>
#include <twist/fiber/test.hpp>

#include <wheels/support/compiler.hpp>

#include <wheels/logging/logging.hpp>

using namespace twist::fiber;

TEST_SUITE(Fiber) {
  TWIST_FIBER_IMPL_TEST(OneFiber) {
    LOG_SIMPLE("write to log from fiber!");
  }

  struct TestException {
  };

  TWIST_FIBER_IMPL_TEST(Exceptions) {
    auto bar = []() {
      throw TestException();
    };

    auto foo = [&]() {
      bar();
    };

    ASSERT_THROW(foo(), TestException);
  }

  SIMPLE_TEST(PingPong) {
    int count = 0;

    auto finn = [&count]() {
      for (size_t i = 0; i < 10; ++i) {
        ++count;
        Yield();
        ASSERT_EQ(count, 0);
      }
      ++count;
    };

    auto jake = [&count]() {
      for (size_t i = 0; i < 10; ++i) {
        --count;
        Yield();
        ASSERT_EQ(count, 1);
      }
    };

    twist::fiber::RunScheduler([&]() {
      Spawn(finn);
      Spawn(jake);
    });
  }

  SIMPLE_TEST(RoundRobin) {
    static const size_t kFibers = 5;
    static const size_t kRounds = 5;

    size_t next = 0;

    auto routine = [&](size_t k) {
      for (size_t i = 0; i < kRounds; ++i) {
        ASSERT_EQ(next, k);
        next = (next + 1) % kFibers;
        twist::fiber::Yield();
      }
    };

    RunScheduler([&]() {
      for (size_t k = 0; k < kFibers; ++k) {
        Spawn([&, k](){ routine(k); });
      }
    });
  }

  SIMPLE_TEST(Sleep) {
    static const size_t kFibers = 10;

    auto main = [&]() {
      for (size_t k = 0; k < kFibers; ++k) {
        auto routine = [k]() {
          auto sleep_duration = k * k * std::chrono::milliseconds(10);

          auto start = FiberSteadyClock::now();

          SleepFor(sleep_duration);

          auto elapsed = FiberSteadyClock::now() - start;

          ASSERT_TRUE(elapsed > sleep_duration);
        };

        Spawn(routine);
      }
    };

    RunScheduler(main);
  }

  /*
  void recurse() {
    recurse();
  }

  SIMPLE_TEST(StackOverflow) {
    RunScheduler(recurse);
  }
  */

  SIMPLE_TEST(ThreadLike) {
    bool done = false;

    auto child_routine = [&]() {
      for (size_t i = 0; i < 10; ++i) {
        Yield();
      }
      done = true;
    };

    RunScheduler([&]() {
      ThreadLike child(child_routine);
      ASSERT_FALSE(done);
      ThreadLike moved(std::move(child));
      moved.Join();
      ASSERT_TRUE(done);
    });
  }

  SIMPLE_TEST(TestWaitQueue) {
    WaitQueue wait_queue;
    size_t step = 0;

    auto foo = [&]() {
      ASSERT_EQ(step, 1);
      wait_queue.WakeOne();
      ASSERT_EQ(step, 1);
      ++step;
      Yield();
      ASSERT_EQ(step, 3);
    };

    auto main = [&]() {
      Spawn(foo);
      ++step;
      wait_queue.Park();
      ASSERT_EQ(step, 2);
      ++step;
      Yield();
    };

    RunScheduler(main);
  }

  SIMPLE_TEST(LiveLock) {
    // Exercise for curious reader: insert several Yield-s into this test
    // to produce deterministic livelock

    size_t count = 0;

    auto routine = [&]() {
      for (size_t i = 0; i < 3; ++i) {
        // lock
        while (count++ > 0) {
          --count;
        }

        // critical section

        // unlock
        --count;
      }
    };

    RunScheduler([&]() {
      Spawn(routine);
      Spawn(routine);
    });
  }

  SIMPLE_TEST(MutualExclusion) {
    Mutex mutex;
    bool critical = false;

    auto routine = [&]() {
      for (size_t i = 0; i < 10; ++i) {
        mutex.Lock();
        ASSERT_FALSE(critical);
        critical = true;
        for (size_t j = 0; j < 3; ++j) {
          Yield();
        }
        ASSERT_TRUE(critical);
        critical = false;
        mutex.Unlock();
        Yield();
      }
    };

    RunScheduler([&]() {
      Spawn(routine);
      Spawn(routine);
    });
  }

  SIMPLE_TEST(DeadLock) {
    Mutex a;
    Mutex b;

    auto first = [&]() {
      a.Lock();
      Yield();
      b.Lock();
    };

    auto second = [&]() {
      b.Lock();
      Yield();
      a.Lock();
    };

    WHEELS_UNUSED(second);

    RunScheduler([&]() {
      Spawn(first);
      // Uncomment to trigger deadlock
      // Spawn(second);
    });
  }
}

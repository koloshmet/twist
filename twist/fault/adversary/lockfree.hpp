#pragma once

#include <twist/fault/adversary/adversary.hpp>

namespace twist {
namespace fault {

IAdversaryPtr CreateLockFreeAdversary();

}  // namespace fault
}  // namespace twist

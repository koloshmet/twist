#pragma once

#include <twist/fault/random/integer.hpp>

#include <atomic>

namespace twist {
namespace fault {

class RandomEvent {
 public:
  RandomEvent(size_t freq) : freq_(freq) {
    Init();
  }

  // Wait-free
  bool Test() {
    if (left_.fetch_sub(1, std::memory_order_relaxed) == 1) {
      // Last tick
      Reset();
      return true;
    }
    return false;
  }

  void Reset() {
    left_.store(RandomUInteger(1, freq_), std::memory_order_relaxed);
  }

 private:
  void Init() {
    left_ = 1;  // TODO
  }

 private:
  size_t freq_;
  std::atomic<int> left_{0};
};

}  // namespace fault
}  // namespace twist

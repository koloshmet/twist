#include <twist/fault/random/source.hpp>

#if defined(TWIST_FIBERS)

#include <twist/fiber/runtime/scheduler.hpp>

namespace twist::fault {

// Deterministic randomness for fibers

uint64_t RandomUInt64() {
  return fiber::GetCurrentScheduler()->GenerateRandomNumber();
}

}  // namespace twist::fault

#else

#include <wheels/support/random.hpp>

namespace twist::fault {

uint64_t RandomUInt64() {
  return wheels::RandomUInteger((uint64_t)-1);
}

}  // namespace twist::fault

#endif

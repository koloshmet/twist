#pragma once

#include <cstdint>

namespace twist::fault {

uint64_t RandomUInt64();

}  // namespace twist::fault
#include <twist/fault/wrappers/thread.hpp>

// TODO: move to fault
#include <twist/test/util/affinity.hpp>

#include <twist/fault/adversary/adversary.hpp>

namespace twist {
namespace fault {

FaultyThread::FaultyThread(ThreadRoutine routine)
    : impl_([routine = std::move(routine)]() mutable {
        SetTestThreadAffinity();
        GetAdversary()->Enter();
        routine();
        GetAdversary()->Exit();
      }) {
}

}  // namespace fault
}  // namespace twist
#include <twist/fiber/report/stacktrace.hpp>

#include <wheels/support/compiler.hpp>

#if defined(TWIST_PRINT_STACKS)

#if LINUX
#define BACKWARD_HAS_BFD 1
#endif

#include <wheels/support/terminal.hpp>

#include <backward.hpp>

using namespace backward;

namespace twist {

#if APPLE

// OSX
void PrintStackTrace(std::ostream& out) {
  static const size_t kDepth = 64;

  StackTrace st;
  st.load_here(kDepth);

  TraceResolver tr;
  tr.load_stacktrace(st);
  for (size_t i = 0; i < st.size(); ++i) {
    ResolvedTrace trace = tr.resolve(st[i]);
    out << "#"
        << i
        //<< " " << trace.object_filename
        << " " << trace.object_function << " [" << trace.addr << "]"
        << std::endl;
  }
}

#elif LINUX

static const std::vector<std::string> kFilenameBlackList{"include/c++", "twist",
                                                         "_deps/"};

static bool IsProbablyUserFrame(const ResolvedTrace& trace) {
  for (const auto& pattern : kFilenameBlackList) {
    if (trace.source.filename.find(pattern) != std::string::npos) {
      return false;
    }
  }
  return true;
}

void PrintStackTrace(std::ostream& out) {
  static const size_t kDepth = 64;

  StackTrace st;
  st.load_here(kDepth);

  TraceResolver tr;
  tr.load_stacktrace(st);

  size_t frames_printed = 0;

  SnippetFactory snippets;
  for (size_t i = 0; i < st.size(); ++i) {
    backward::ResolvedTrace trace = tr.resolve(st[i]);

    if (!IsProbablyUserFrame(trace)) {
      continue;
    }

    auto lines =
        snippets.get_snippet(trace.source.filename, trace.source.line, 3);

    if (lines.empty()) {
      continue;
    }

    if (frames_printed > 0) {
      out << std::endl;
    }

    out << "#"
        << i
        //<< " " << trace.object_filename
        << " " << trace.object_function << " [" << trace.addr << "]"
        << std::endl;

    for (auto& line : lines) {
      std::string prefix = "  ";
      if (line.first == trace.source.line) {
        prefix = "> ";
        out << wheels::terminal::Magenta();
      }
      out << prefix << line.first << ": " << line.second << std::endl;
      out << wheels::terminal::Reset();
    }

    ++frames_printed;
  }

  out << std::endl << std::string(30, '-') << std::endl;
}

#else

void PrintStackTrace(std::ostream& out) {
  WHEELS_UNUSED(out);
  // Nop
}

#endif

}  // namespace twist

#else

namespace twist {

void PrintStackTrace(std::ostream& out) {
  WHEELS_UNUSED(out);
  // Nop
}

}  // namespace twist

#endif

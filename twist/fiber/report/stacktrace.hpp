#pragma once

#include <ostream>

namespace twist {

void PrintStackTrace(std::ostream& out);

}  // namespace twist

#pragma once

namespace twist::fiber {

uint64_t GenerateRandomNumber();

}  // namespace twist::fiber

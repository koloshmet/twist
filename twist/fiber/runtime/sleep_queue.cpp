#include <twist/fiber/runtime/sleep_queue.hpp>

namespace twist {
namespace fiber {

void SleepQueue::Put(Fiber* f, std::chrono::milliseconds d) {
  queue_.push({f, ToDeadLine(d)});
}

bool SleepQueue::IsEmpty() const {
  return queue_.empty();
}

Fiber* SleepQueue::Poll(TimePoint now) {
  if (IsEmpty()) {
    return nullptr;
  }
  Entry e = queue_.top();
  if (e.deadline_ <= now) {
    queue_.pop();
    return e.fiber_;
  } else {
    return nullptr;
  }
}

SleepQueue::TimePoint SleepQueue::NextDeadLine() const {
  WHEELS_VERIFY(!IsEmpty(), "Sleep queue is empty");
  return queue_.top().deadline_;
}

SleepQueue::TimePoint SleepQueue::ToDeadLine(std::chrono::milliseconds d) {
  return Clock::now() + d;
}

}  // namespace fiber
}  // namespace twist

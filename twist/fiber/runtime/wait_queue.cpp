#include <twist/fiber/runtime/wait_queue.hpp>

#include <twist/fiber/runtime/scheduler.hpp>

#if defined(TWIST_FAULTY)
#include <twist/fault/random/helpers.hpp>
#endif

namespace twist {
namespace fiber {

static inline void Suspend(std::string_view where) {
  GetCurrentScheduler()->Suspend(where);
}

static inline void Resume(Fiber* waiter) {
  GetCurrentScheduler()->Resume(waiter);
}

WaitQueue::WaitQueue(const std::string& descr) : descr_(descr) {
}

void WaitQueue::Park() {
  Fiber* caller = GetCurrentFiber();
  waiters_.PushBack(caller);
  Suspend(/*where=*/descr_);
}

void WaitQueue::WakeOne() {
  if (waiters_.IsEmpty()) {
    return;
  }
#if defined(TWIST_FAULTY)
  Fiber* f = fault::UnlinkRandomItem(waiters_);
#else
  Fiber* f = waiters_.PopFront();
#endif
  Resume(f);
}

void WaitQueue::WakeAll() {
#if defined(TWIST_FAULTY)
  auto shuffled = fault::ShuffleToVector(waiters_);
  for (Fiber* f : shuffled) {
    Resume(f);
  }
#else
  while (!waiters_.IsEmpty()) {
    Fiber* f = waiters_.PopFront();
    Resume(f);
  }
#endif
}

}  // namespace fiber
}  // namespace twist

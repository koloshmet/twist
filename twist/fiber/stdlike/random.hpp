#pragma once

#include <twist/fiber/runtime/random.hpp>

#include <limits>

namespace twist::fiber {

class RandomDevice {
 public:
  using result_type = unsigned int;

  result_type operator()() {
    return GenerateRandomNumber();
  }

  // Characteristics

  double entropy() const noexcept {
    return 32;
  }

  static constexpr result_type min() {
    return 0;
  }

  static constexpr result_type max() {
    return std::numeric_limits<result_type>::max();
  }
};

}  // namespace twist::fiber

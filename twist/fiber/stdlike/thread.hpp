#pragma once

#include <twist/fiber/runtime/api.hpp>

#include <wheels/support/panic.hpp>

#include <memory>

namespace twist {
namespace fiber {

class ThreadLike {
 public:
  using id = FiberId;  // NOLINT

 public:
  ThreadLike(FiberRoutine routine);
  ~ThreadLike();

  ThreadLike(ThreadLike&& that);
  ThreadLike& operator=(ThreadLike&& that);

  void Join();

  bool Joinable() const;

  // std::thread interface

  void join() {  // NOLINT
    Join();
  }

  bool joinable() const {  // NOLINT
    return Joinable();
  }

  void detach() {  // NOLINT
    WHEELS_PANIC("detach not implemented");
  }

 private:
  class Impl;
  std::unique_ptr<Impl> pimpl_;
};

}  // namespace fiber
}  // namespace twist

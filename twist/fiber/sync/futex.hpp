#pragma once

#include <twist/fiber/runtime/wait_queue.hpp>

namespace twist {
namespace fiber {

WaitQueue& WaitQueueFor(void* address);

}  // namespace fiber
}  // namespace twist

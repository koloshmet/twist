#pragma once

#include <twist/fiber/runtime/api.hpp>

#include <wheels/test/test_framework.hpp>

#define TWIST_FIBER_IMPL_TEST(name)                       \
  void FiberTestRoutine##name(void);                      \
  SIMPLE_TEST(name) {                                     \
    ::twist::fiber::RunScheduler(FiberTestRoutine##name); \
  }                                                       \
  void FiberTestRoutine##name()

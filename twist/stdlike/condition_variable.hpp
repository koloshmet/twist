#pragma once

#if defined(TWIST_FAULTY)

#include <twist/fault/wrappers/condvar.hpp>

namespace twist::stdlike {

using condition_variable = fault::FaultyCondVar;  // NOLINT

}  // namespace twist::stdlike

#else

#include <condition_variable>

namespace twist::stdlike {

using std::condition_variable;

}  // namespace twist::stdlike

#endif

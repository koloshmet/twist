#pragma once

#include <random>

#if defined(TWIST_FIBERS)

#include <twist/fiber/stdlike/random.hpp>

namespace twist::stdlike {

using random_device = twist::fiber::RandomDevice;

}  // namespace twist::stdlike

#else

namespace twist::stdlike {

using random_device = std::random_device;

}  // namespace twist::stdlike

#endif
#pragma once

#if defined(TWIST_FIBERS)

#include <twist/fiber/runtime/api.hpp>

namespace twist::strand {

template <typename F>
void Execute(F f) {
  twist::fiber::RunScheduler([f]() {
    f();
  });
}

}  // namespace twist::strand

#else

namespace twist::strand {

template <typename F>
void Execute(F f) {
  f();
}

}  // namespace twist::strand

#endif

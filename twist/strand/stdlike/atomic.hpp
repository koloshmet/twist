#pragma once

#if defined(TWIST_FIBERS)

#include <twist/fiber/stdlike/atomic.hpp>

namespace twist::strand::stdlike {

template <typename T>
using atomic = fiber::Atomic<T>;

}  // namespace twist::strand::stdlike

#else

#include <twist/thread/stdlike/atomic.hpp>

namespace twist::strand::stdlike {

// TODO: using atomic = std::atomic<T>
template <typename T>
using atomic = twist::thread::FutexedAtomic<T>;

}  // namespace twist::strand::stdlike

#endif


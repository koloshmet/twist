#include <twist/strand/tls.hpp>

#include <twist/fiber/runtime/scheduler.hpp>

namespace twist {
namespace strand {

// NB: destroyed before TLSManager
static thread_local ManagedTLS tls;

TLS& TLSManager::AccessTLS() {
#if defined(TWIST_FIBERS)
  return fiber::GetCurrentFiber()->AccessFLS();
#else
  return tls.Access();
#endif
}

}  // namespace strand
}  // namespace twist

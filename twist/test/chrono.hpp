#pragma once

#include <twist/strand/stdlike/thread.hpp>
#include <twist/strand/chrono.hpp>

namespace twist::test {

using strand::stdlike::this_thread::sleep_for;

using twist::strand::SteadyClock;

}  // namespace twist::test

#pragma once

#include <twist/fault/random/integer.hpp>

namespace twist::test {

// [0, max]
inline uint64_t RandomUInteger(uint64_t max) {
  return fault::RandomUInteger(max);
}

// [lo, hi]
inline uint64_t RandomUInteger(uint64_t lo, uint64_t hi) {
  return fault::RandomUInteger(lo, hi);
}

inline bool Random2() {
  return RandomUInteger(2) == 0;
}

}  // namespace twist::test

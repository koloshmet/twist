#include <twist/test/run.hpp>

#if defined(TWIST_FAULTY)
#include <twist/fault/adversary/adversary.hpp>
#endif

#if defined(TWIST_FIBERS)

#include <twist/fiber/runtime/api.hpp>

namespace twist {

void Run(wheels::UniqueFunction<void()> main) {
  fiber::RunScheduler(std::move(main));

#if defined(TWIST_FAULTY)
  fault::GetAdversary()->PrintReport();
#endif
}

}  // namespace twist

#else

namespace twist {

void Run(wheels::UniqueFunction<void()> main) {
  main();

#if defined(TWIST_FAULTY)
  fault::GetAdversary()->PrintReport();
#endif
}

}  // namespace twist

#endif

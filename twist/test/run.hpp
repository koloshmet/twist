#pragma once

#include <wheels/support/function.hpp>

namespace twist {

void Run(wheels::UniqueFunction<void()> main);

}  // namespace twist

#pragma once

#include <twist/fault/adversary/adversary.hpp>

#include <twist/stdlike/mutex.hpp>

namespace twist::test::util {

template <typename T>
class ReportProgressFor {
  class Proxy {
   public:
    Proxy(T* object) : object_(object) {
    }

    T* operator->() {
      return object_;
    }

    ~Proxy() {
      twist::fault::GetAdversary()->ReportProgress();
    }

   private:
    T* object_;
  };

 public:
  Proxy operator->() {
    return {&object_};
  }

 private:
  T object_;
};

inline void ReportProgress() {
  fault::GetAdversary()->ReportProgress();
}

struct EnablePark {
  EnablePark() {
    fault::GetAdversary()->EnablePark();
  }

  ~EnablePark() {
    fault::GetAdversary()->DisablePark();
  }
};

struct DisablePark {
  DisablePark() {
    fault::GetAdversary()->DisablePark();
  }

  ~DisablePark() {
    fault::GetAdversary()->EnablePark();
  }
};

}  // namespace twist::test::util

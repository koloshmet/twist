#include <twist/thread/native_futex.hpp>

#if LINUX

#include <unistd.h>
#include <sys/syscall.h>
#include <linux/futex.h>

namespace {

// There is no glibc wrapper for 'futex' syscall

int futex(unsigned int* uaddr, int op, int val, const struct timespec* timeout,
          int* uaddr2, int val3) {
  return syscall(SYS_futex, uaddr, op, val, timeout, uaddr2, val3);
}

}  // namespace

namespace twist {
namespace thread {

int PlatformWait(uint32_t* addr, uint32_t expected) {
  return futex(addr, FUTEX_WAIT_PRIVATE, expected, nullptr, nullptr, 0);
}

int PlatformWake(uint32_t* addr, size_t count) {
  return futex(addr, FUTEX_WAKE_PRIVATE, (int)count, nullptr, nullptr, 0);
}

}  // namespace thread
}  // namespace twist

#elif APPLE

extern "C" int __ulock_wait(uint32_t operation, void *addr, uint64_t value,
                            uint32_t timeout); /* timeout is specified in microseconds */
extern "C" int __ulock_wake(uint32_t operation, void *addr, uint64_t wake_value);

#define UL_COMPARE_AND_WAIT				1
#define ULF_WAKE_ALL					0x00000100


namespace twist {
namespace thread {

int PlatformWait(uint32_t* addr, uint32_t expected) {
  return __ulock_wait(UL_COMPARE_AND_WAIT, addr, expected, 0);
}

int PlatformWake(uint32_t* addr, size_t count) {
  return __ulock_wake(UL_COMPARE_AND_WAIT | ((count == 1) ? 0 : ULF_WAKE_ALL), addr, 0);
}

}  // namespace thread
}  // namespace twist


#else

#pragma message("Futex is not implemented on current platform")

// TODO: emulate futex with mutex+condvar

#endif

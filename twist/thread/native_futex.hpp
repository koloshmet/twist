#pragma once

#include <cstdint>
#include <cstddef>
#include <climits>
#include <atomic>

#include <wheels/support/assert.hpp>

// https://en.wikipedia.org/wiki/Futex

namespace twist {
namespace thread {

// Simple wrappers around futex syscall on Linux

int PlatformWait(uint32_t* addr, uint32_t expected);

int PlatformWake(uint32_t* addr, size_t count);

}  // namespace thread
}  // namespace twist

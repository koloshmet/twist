#pragma once

namespace twist {
namespace thread {

// Pin current thread to a single cpu core

void PinThisThread(int desired_cpu);

}  // namespace thread
}  // namespace twist

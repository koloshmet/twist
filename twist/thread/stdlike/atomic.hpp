#pragma once

#include <twist/thread/native_futex.hpp>

// INT_MAX
#include <cstdint>

namespace twist::thread {

template <typename T>
class FutexedAtomic : public std::atomic<T> {  // NOLINT
 public:
  FutexedAtomic() noexcept = default;

  explicit FutexedAtomic(T initial_value) : std::atomic<T>(initial_value) {
  }

  // NOLINTNEXTLINE
  void wait(T old) {
    // https://eel.is/c++draft/atomics.types.generic#lib:atomic,wait
    while (this->load() == old) {
      FutexWait(old);
    }
  }

  // NOLINTNEXTLINE
  void notify_one() {
    FutexWakeOne();
  }

  // NOLINTNEXTLINE
  void notify_all() {
    FutexWakeAll();
  }

  // Direct access to futex syscall

  void FutexWait(T old);
  void FutexWakeOne();
  void FutexWakeAll();

 private:
  uint32_t* FutexAddr();
};

// Support for std::atomic<T>::wait from C++20

template <typename T>
uint32_t* FutexedAtomic<T>::FutexAddr() {
  static_assert(std::is_same<T, uint32_t>::value, "Not supported");
  return reinterpret_cast<uint32_t*>(this);
}

template <typename T>
void FutexedAtomic<T>::FutexWait(T old) {
  thread::PlatformWait(FutexAddr(), old);
}

template <typename T>
void FutexedAtomic<T>::FutexWakeOne() {
  thread::PlatformWake(FutexAddr(), 1);
}

template <typename T>
void FutexedAtomic<T>::FutexWakeAll() {
  thread::PlatformWake(FutexAddr(), INT_MAX);
}

}  // namespace twist::thread
